package form_user;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import form_user.Form_cad_user;
import form_user.Form_edit_user;
import form_user.Form_delete_user;
import form_user.Users_list;

import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.Usuario;

public class Users_list extends JFrame{
	
	private JTable tabela;
	private JScrollPane sPnl;
	private JPanel painelFundo;
	private JButton btnAdd;
	private JButton btnEdita;
	private JButton btnDeleta;
	
	public Users_list(){
		
		this.setBounds(250, 250, 500, 300);
		
		
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Gerenciamento de Usu�rios");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
        
		
		this.btnAdd = new JButton("Cadastrar");
		this.btnAdd.setBounds(50, 45, 150, 30);
		this.btnEdita = new JButton("Editar");
		this.btnEdita.setBounds(50, 90, 150, 30);
		this.btnDeleta = new JButton("Excluir");
		this.btnDeleta.setBounds(50, 135, 150, 30);
		
		this.add(this.btnAdd);
		this.add(this.btnEdita);
		this.add(this.btnDeleta);
		
		this.getContentPane().add(painelFundo);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_cad_user();
				dispose();
			}
		});
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_edit_user();
				dispose();
			}
		});
		
		btnDeleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_delete_user();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Users_list();
	}

}
