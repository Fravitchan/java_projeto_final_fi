package form_user;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import form_user.Login;
import form_user.Adm;
import database.Usuario;


public class Login extends JFrame{

	private JLabel			lblUsuario 	= new JLabel("Usu�rio:");
	private JTextField 		txtUsuario 	= new JTextField();
	private JLabel			lblSenha 	= new JLabel("Senha:");
	private JPasswordField 	txtSenha 	= new JPasswordField();
	private JButton			btnEnviar 	= new JButton("Acessar");
	private JLabel			lblMsg 		= new JLabel("");
	
	public Login() {
		this.setTitle("Biblioteca - Login");
		this.setBounds(250, 250, 300, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lblUsuario.setBounds(  15,  5, 100, 30);
		txtUsuario.setBounds(110,  5, 150, 30);
		lblSenha.setBounds	(  15, 40, 100, 30);
		txtSenha.setBounds	(110, 40, 150, 30);
		btnEnviar.setBounds	(110, 75, 150, 30);
		lblMsg.setBounds	(  15,110, 300, 30);
		
		this.add(lblUsuario);
		this.add(txtUsuario);
		this.add(lblSenha);
		this.add(txtSenha);
		this.add(btnEnviar);
		this.add(lblMsg);
		
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario("", txtUsuario.getText(), txtUsuario.getText(), txtSenha.getText(), "");
				if (usuario.checkLogin() == true){
					new Adm();
					dispose();
				}else{
					lblMsg.setText("ERRO: Usu�rio ou Senha Inv�lidos!");
				}
				
			}
		});
		
		this.setVisible(true);
		
		
	}
	public static void main(String[] args) {
		new Login();
	}
	
}
