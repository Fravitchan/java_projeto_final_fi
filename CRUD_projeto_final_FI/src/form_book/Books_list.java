package form_book;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import form_book.Form_cad_book;
import form_book.Form_delete_book;
import form_book.Form_edit_book;
import form_book.Books_list;


import java.awt.GridLayout;
import java.sql.ResultSet;
import java.sql.SQLException;

import database.Livro;

public class Books_list extends JFrame{

	private JTable tabela;
	private JScrollPane sPnl;
	private JPanel painelFundo;
	private JButton btnAdd;
	private JButton btnEdita;
	private JButton btnDeleta;
	/*//private DefaultTableModel modelo = new DefaultTableModel();
	
	public void setDados(DefaultTableModel modelo){
		
		Livro livro = new Livro("", "", "", "", "", "", "");
		ResultSet rs = livro.listall();
		this.modelo.setNumRows(0);

		try {
			while(rs.next())
			{
				this.modelo.addRow(new Object[]{rs.getString("titulo"), ""+rs.getString("autor"), ""+rs.getString("editora"), ""+rs.getString("genero"), ""+rs.getString("status"), ""+rs.getString("cpf_cliente")});
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Books_list(){
		
		this.setBounds(250, 250, 500, 300);
		
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Gerenciamento dos Livros");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(600, 400);
		
		this.tabela = new JTable(this.modelo);
		this.modelo.addColumn("ID");
		this.modelo.addColumn("Nome");
		this.modelo.addColumn("G�nero");
		this.modelo.addColumn("Sinopse");
		this.modelo.addColumn("Ano");
		this.tabela.getColumnModel().getColumn(0).setPreferredWidth(10);
		tabela.getColumnModel().getColumn(1).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        tabela.getColumnModel().getColumn(2).setPreferredWidth(120);
        
        this.setDados(modelo);
        
		this.sPnl = new JScrollPane(this.tabela);
		this.painelFundo.add(sPnl);
		
		this.btnAdd = new JButton("Cadastrar");
		this.btnAdd.setBounds(360, 45, 150, 30);
		this.btnEdita = new JButton("Editar");
		this.btnEdita.setBounds(360, 90, 150, 30);
		this.btnDeleta = new JButton("Excluir");
		this.btnDeleta.setBounds(360, 135, 150, 30);
		
		this.add(this.btnAdd);
		this.add(this.btnEdita);
		this.add(this.btnDeleta);
		
		this.getContentPane().add(painelFundo);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_cad_book();
				dispose();
			}
		});
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_edit_book();
				dispose();
			}
		});
		
		btnDeleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_delete_book();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Books_list();
	}*/
	
public Books_list(){
		
		this.setBounds(250, 250, 500, 300);
		
		
		painelFundo = new JPanel();
		painelFundo.setLayout(new GridLayout(0, 2));
		
		this.setTitle("Livros");
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(500, 300);
        
		
		this.btnAdd = new JButton("Cadastrar");
		this.btnAdd.setBounds(50, 45, 150, 30);
		this.btnEdita = new JButton("Editar");
		this.btnEdita.setBounds(50, 90, 150, 30);
		this.btnDeleta = new JButton("Excluir");
		this.btnDeleta.setBounds(50, 135, 150, 30);
		
		this.add(this.btnAdd);
		this.add(this.btnEdita);
		this.add(this.btnDeleta);
		
		this.getContentPane().add(painelFundo);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_cad_book();
				dispose();
			}
		});
		
		btnEdita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_edit_book();
				dispose();
			}
		});
		
		btnDeleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Form_delete_book();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Books_list();
	}

	
}
