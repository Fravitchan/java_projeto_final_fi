package form_book;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import form_book.Form_delete_book;
import form_book.Books_list;

import database.Livro;

public class Form_delete_book extends JFrame{

	private JLabel			lbl_Id 	= new JLabel("ID:");
	private JTextField 		txt_Id 	= new JTextField();
	private JButton			btnEnviar 	= new JButton("Excluir");
	private JButton			btnVoltar 	= new JButton("Voltar");
	private JLabel			lblMsg 		= new JLabel("_______________________________________");
	
	public Form_delete_book(){
		this.setTitle("Apagar Livro");
		this.setBounds(250, 250, 300, 150);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lbl_Id.setBounds	(  5,  5, 100, 30);
		txt_Id.setBounds	(110,  5, 150, 30);
		btnEnviar.setBounds	(140, 40, 120, 30);
		btnVoltar.setBounds	(  5, 40, 120, 30);
		lblMsg.setBounds	(  5, 75, 300, 30);
		
		this.add(lbl_Id);
		this.add(txt_Id);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Livro livro = new Livro(txt_Id.getText(), "", "", "", "", "", "");
				if (livro.delete() == true){
					new Books_list();
					dispose();
				}else{
					lblMsg.setText("N�o foi poss�vel apagar o livro.");
				}
				
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Books_list();
				dispose();
			}
		});
		
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new Form_delete_book();
	}
	
}
