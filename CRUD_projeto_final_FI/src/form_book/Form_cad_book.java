package form_book;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import form_book.Form_cad_book;
import form_book.Books_list;
import database.Livro;

public class Form_cad_book extends JFrame{
	
	private JLabel lbl_Id_book = new JLabel("Id:");
	private JTextField txt_Id_book = new JTextField();
	private JLabel lbl_Titulo = new JLabel("T�tulo livro:");
	private JTextField txt_Titulo = new JTextField();
	private JLabel lbl_Autor = new JLabel("Autor:");
	private JTextField txt_Autor = new JTextField();
	private JLabel lbl_Editora = new JLabel("Editora:");
	private JTextField txt_Editora = new JTextField();
	private JLabel lbl_Genero = new JLabel("G�nero:");
	private JTextField txt_Genero = new JTextField();
	private JLabel lbl_Status = new JLabel("Status:");
	private JTextField txt_Status = new JTextField();
	private JLabel lbl_Cpf_cliente = new JLabel("CPF do cliente:");
	private JTextField txt_Cpf_cliente = new JTextField();
	private JButton	btnEnviar 	= new JButton("Cadastrar");
	private JButton	btnVoltar 	= new JButton("Voltar");
	private JLabel	lblMsg 		= new JLabel("");
	
	public Form_cad_book(){
		this.setTitle("Cadastrar Livro");
		this.setBounds(250, 400, 340, 340);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(null);
		
		lbl_Id_book.setBounds(  15,   5, 100, 30);
		txt_Id_book.setBounds(110,   5, 150, 30);
		lbl_Titulo.setBounds(  15,   40, 100, 30);
		txt_Titulo.setBounds(110,   40, 150, 30);
		lbl_Autor.setBounds(  15,   75, 100, 30);
		txt_Autor.setBounds(110,   75, 150, 30);
		lbl_Editora.setBounds(  15,  110, 100, 30);
		txt_Editora.setBounds(110,  110, 150, 30);
		lbl_Genero.setBounds(  15,  145, 100, 30);
		txt_Genero.setBounds(110,  145, 150, 30);
		lbl_Status.setBounds(  15, 180, 100, 30);
		txt_Status.setBounds(110, 180, 150, 30);
		lbl_Cpf_cliente.setBounds(  15, 215, 100, 30);
		txt_Cpf_cliente.setBounds(110, 215, 150, 30);
		btnEnviar.setBounds(140, 250, 120, 30);
		btnVoltar.setBounds(  15, 250, 120, 30);
		lblMsg.setBounds(  15, 290, 300, 30);
		
		this.add(lbl_Id_book);
		this.add(txt_Id_book);
		this.add(lbl_Titulo);
		this.add(txt_Titulo);
		this.add(lbl_Autor);	
		this.add(txt_Autor);
		this.add(lbl_Editora);
		this.add(txt_Editora);
		this.add(lbl_Genero);
		this.add(txt_Genero);
		this.add(lbl_Status);
		this.add(txt_Status);
		this.add(lbl_Cpf_cliente);
		this.add(txt_Cpf_cliente);
		this.add(btnEnviar);
		this.add(btnVoltar);
		this.add(lblMsg);
		
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Livro livro = new Livro(txt_Id_book.getText(), txt_Titulo.getText(), txt_Autor.getText(), txt_Editora.getText(), txt_Genero.getText(), txt_Status.getText(), txt_Cpf_cliente.getText());
				if (livro.save() == true){
					new Books_list();
					dispose();
				}else{
					lblMsg.setText("Falha do Cadastro");
				}
			}
		});
		
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Books_list();
				dispose();
			}
		});
		
			this.setVisible(true);
		}
	
		public static void main(String[] args) {
			new Form_cad_book();
		}
		
				 
											
	}


