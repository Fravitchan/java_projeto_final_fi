package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Livro {

	private String id_book;
	private String titulo;
	private String autor;
	private String editora;
	private String genero; 
	private String status; 
	private String cpf_cliente;
	
	private String tableName	= "biblioteca.livros";
	private String fieldsName 	= "id_book, titulo, autor, editora, genero, status, cpf_cliente";
	private String fieldKey		= "id_book";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	public Livro(String id_book, String titulo, String autor, String editora, String genero, String status, String cpf_cliente){
		this.id_book = id_book;
		this.titulo = titulo;
		this.autor = autor;
		this.editora = editora;
		this.genero = genero;
		this.status = status;
		this.cpf_cliente = cpf_cliente;
	}
	
	private String[] toArray(){
		return(
				new String[]{
						this.id_book,
						this.titulo,
						this.autor,
						this.editora,
						this.genero,
						this.status,
						this.cpf_cliente,
				}
		);
	}
	
	public boolean save(){
		if (this.id_book == ""){
			this.id_book = "0";
			return dbQuery.insert(this.toArray());
		}else{
			return dbQuery.update(this.toArray());
		}
	}
	
	public boolean delete() {
		if (this.id_book != ""){
			return dbQuery.delete(this.toArray());
		}
		return false;
	}
	
	/*public boolean checkLogin() {
	
	ResultSet rs =	dbQuery.select(" (nome = '"+this.nome+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
 	try {
 		return( rs.next() );
	 /*
		if ( rs.next() ){
		 return( true );
	 	}	else {
		 	return( false );
	 	}
	 
 	} catch (SQLException e) {
		e.printStackTrace();
	}
 	return( false );
}*/

		public ResultSet listall() {
			return(dbQuery.select(""));
		}
		
		public String getId_book() {
			return id_book;
		}
		public void setId_book(String id_book) {
			this.id_book = id_book;
		}
		public String getTitulo() {
			return titulo;
		}
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		public String getAutor(){
			return autor;
		}
		public void setAutor(String autor){
			this.autor =  autor;
		}
		public String getEditora(){
			return editora;
		}
		public void setEditora(String editora){
			this.editora =  editora;
		}
		public String getGenero() {
			return genero;
		}
		public void setGenero(String genero) {
			this.genero = genero;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getCpf_cliente() {
			return cpf_cliente;
		}
		public void setCpf_cliente(String cpf_cliente) {
			this.cpf_cliente = cpf_cliente;
		}
	
}
