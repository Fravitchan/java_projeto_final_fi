package database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Usuario {
	
	//variáveis dos campos da tabela Users.
	private String id_user;
	private String name_user;
	private String email;
	private String senha;
	private String nivel;
	
	private String tableName	= "biblioteca.users";
	private String fieldsName 	= "id_user, name_user, email, senha, nivel";
	private String fieldKey		= "id_user";
	
	DBQuery dbQuery = new DBQuery(tableName, fieldsName, fieldKey);
	
	public Usuario(String id_user, String name_user, String email, String  senha, String nivel) {
		this.id_user   = id_user;
		this.name_user = name_user;
		this.email     = email;
		this.senha     = senha;
		this.nivel     = nivel;
	}
	
	private String[] toArray() {
		return(
			new String[]{
				this.id_user, 
				this.name_user,
				this.email,
				this.senha,
				this.nivel				
			}
		);

	}
	
	public boolean save() {
		if (this.id_user == ""){
			this.id_user = "0";
			return dbQuery.insert(this.toArray());
		}else{
			return dbQuery.update(this.toArray());
		}
	}
	
	public boolean delete() {
		if (this.id_user != ""){
			return dbQuery.delete(this.toArray());
		}
		return false;
	}
	
	public boolean checkLogin() {
		
		ResultSet rs =	dbQuery.select(" (name_user = '"+this.name_user+"' OR email = '"+this.email+"') AND  senha = '"+this.senha+"' ");
	 	try {
	 		return( rs.next() );
		 /*
			if ( rs.next() ){
			 return( true );
		 	}	else {
			 	return( false );
		 	}
		 */
	 	} catch (SQLException e) {
			e.printStackTrace();
		}
	 	return( false );
	}
	
	public ResultSet listall() {
		return(dbQuery.select(""));
	}
	
	public String getId_user() {
		return id_user;
	}
	public void setId_user(String id_user) {
		this.id_user = id_user;
	}
	public String getName_user() {
		return name_user;
	}
	public void setNome(String name_user) {
		this.name_user = name_user;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

}
